package com.xpanxion.java.collections;

import java.util.Objects;

public class Person  implements Comparable<Person> { // Comparable: Used for sorting.

    //
    // Data members
    //

    private int age;
    private String lastname;
    private String firstname;

    //
    // Constructors
    //

    public Person(int age, String firstname, String lastname) {
        this.age = age;
        this.lastname = lastname;
        this.firstname = firstname;
    }

    public Person(Person otherPerson) {  // Copy constructor.
        this.age = otherPerson.age;
        this.lastname = otherPerson.lastname;
        this.firstname = otherPerson.firstname;
    }

    //
    // Accessors
    //

    public int getAge() {
        return this.age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getLastname() {
        return this.lastname;
    }
    public String getFirstname() {
        return this.firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public void setLastname(String lastname) {
       this.lastname = lastname;
    }

    //
    // Overrides
    //

    // Required for the priority queue, older people move to the front of the queue when remove() is called.
    // Used for Collections.sort()
    // Fixes error message: class Collections.Person cannot be cast to class java.lang.
    // Negative < 0 < Postive
    @Override
    public int compareTo(Person p) {
        return this.getAge() - p.getAge();
    }

    @Override
    public String toString() {
        return String.format("[Age: %s, Name: %s %s]", age, firstname, lastname);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && firstname.equals(person.firstname) && lastname.equals(person.lastname);
    }

    @Override
    public int hashCode() {  // Used for Hash* data structures (e.g. prevents dupes in HashSets).
        return Objects.hash(age, firstname, lastname);
    }

    //    @Override
//    protected Object clone() throws CloneNotSupportedException {
//        return super.clone();
//    }
}
